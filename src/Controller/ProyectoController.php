<?php

namespace App\Controller;

use App\Entity\Alumnos;
use App\Entity\Cursos;
use App\Entity\Usuarios;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Json;

class ProyectoController extends AbstractController
{
    /**
     * @Route("/ws/cursos", name="ws_cursos", methods={"GET"})
     */
    public function getAll() : JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $alumnado = $em->getRepository(Cursos::class)->findAll();
        $json = $this->convertirJson($alumnado);
        return $json;
    }
    /**
     * @Route("/ws/alumnado", name="ws_alumnado", methods={"GET"})
     */
    public function getAlumnado() : JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $alumnado = $em->getRepository(Alumnos::class)->findAll();
        $json = $this->convertirJson($alumnado);
        return $json;
    }

    /**
     * @Route("/ws/alumnado/actualizar", name="ws_update_alumnado", methods={"PUT"})
     */
    public function actualizarAlumno(Request $request) : JsonResponse
    {
        // Primero recogemos el JSON que nos han enviado
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $curso = $em->getRepository(Cursos::class)->findOneBy(['id' => (integer) $data['idCurso']]);
        $alu = $em->getRepository(Alumnos::class)->findOneBy(['id' => (integer) $data['id']]);
        // Creamos un nuevo usuario y lo añadimos
        $alu->setCursoId($curso);

        $this->getDoctrine()->getManager()->persist($alu);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(
            ['status' => 'Curso actualizado'],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/ws/alumnado/byCurso", name="ws_find_usuario_from_curso", methods={"POST"})
     */
    public function buscarUsuarioCurso(Request $request) : JsonResponse
    {
        // Primero recogemos el JSON que nos han enviado
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        // Comprobamos que el DNI no está vacío
        $curso = $em->getRepository(Cursos::class)->findOneBy(['id' =>(integer) $data['id']]);
        $alumno = $em->getRepository(Alumnos::class)->findBy(['cursoid' => $curso]);
        $json = $this->convertirJson($alumno);
        return $json;

    }

    /**
     * @Route("/ws/usuario", name="ws_find_usuario", methods={"PUT"})
     */
    public function comprobarUsuario(Request $request) : JsonResponse
    {
        // Primero recogemos el JSON que nos han enviado
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        // Comprobamos que el DNI no está vacío
        if (empty($data['user'])) {
            throw new NotFoundHttpException('Faltan parámetros');
        }
        $curso = $em->getRepository(Alumnos::class)->findOneBy(['name' => $data['user'],'password' => $data['password']]);
        $json = $this->convertirJson($curso);
        return $json;

    }

    /**
     * @Route("/ws/alumnado/foto", name="ws_foto_alumnado", methods={"PUT"})
     */
    public function setAlumnoFotoPerfil(Request $request) :JsonResponse
    {
        // Primero recogemos el JSON que nos han enviado
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $alu = $em->getRepository(Alumnos::class)->findOneBy(['name' => $data['name']]);
        // Creamos un nuevo usuario y lo añadimos
        $alu->setFotoPerfil($data['foto_perfil']);

        $this->getDoctrine()->getManager()->persist($alu);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(
            ['status' => 'Foto actualizada'],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/ws/alumnado/getfoto", name="ws_get_foto_alumnado", methods={"POST"})
     */
    public function getAlumnoFotoPerfil(Request $request) :JsonResponse
    {
        // Primero recogemos el JSON que nos han enviado
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $alu = $em->getRepository(Alumnos::class)->findOneBy(['id' => $data['id']]);
        // Creamos un nuevo usuario y lo añadimos
        $foto_perfil=$alu->getFotoPerfil();
        return new JsonResponse(
            ['foto' => $foto_perfil],
            Response::HTTP_CREATED
        );
    }

    private function convertirJson($object) : JsonResponse
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizar = [new DateTimeNormalizer(), new ObjectNormalizer()];
        $serializer = new Serializer($normalizar, $encoders);
        $normalizado = $serializer->normalize($object, null,
            array(DateTimeNormalizer::FORMAT_KEY => 'Y/m/d'));
        $jsonContent = $serializer->serialize($normalizado, 'json');
        return JsonResponse::fromJsonString($jsonContent, 200);
    }
}
