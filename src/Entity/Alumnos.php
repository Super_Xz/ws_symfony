<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alumnos
 *
 * @ORM\Table(name="Alumnos", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}, indexes={@ORM\Index(name="cursoID", columns={"cursoID"})})
 * @ORM\Entity
 */
class Alumnos
{
    /**
     * Alumnos constructor.
     * @param string $name
     * @param float|null $notaMedia
     * @param string|null $password
     * @param bool|null $admin
     * @param string|null $fotoPerfil
     * @param Cursos $cursoid
     */
    public function __construct(string $name, float $notaMedia, string $password, bool $admin, string $fotoPerfil, \Cursos $cursoid)
    {
        $this->name = $name;
        $this->notaMedia = $notaMedia;
        $this->password = $password;
        $this->admin = $admin;
        $this->fotoPerfil = $fotoPerfil;
        $this->cursoid = $cursoid;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return float|null
     */
    public function getNotaMedia(): float
    {
        return $this->notaMedia;
    }

    /**
     * @param float|null $notaMedia
     */
    public function setNotaMedia(float $notaMedia)
    {
        $this->notaMedia = $notaMedia;
    }

    /**
     * @return string|null
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return bool|null
     */
    public function getAdmin(): bool
    {
        return $this->admin;
    }

    /**
     * @param bool|null $admin
     */
    public function setAdmin(bool $admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return string|null
     */
    public function getFotoPerfil(): string
    {
        //var_dump(stream_get_contents($this->fotoPerfil));
        return stream_get_contents($this->fotoPerfil);
    }

    /**
     * @param string|null $fotoPerfil
     */
    public function setFotoPerfil(string $fotoPerfil)
    {
        var_dump($this->fotoPerfil);
        var_dump($fotoPerfil);
        $this->fotoPerfil = $fotoPerfil;
    }

    /**
     * @return Cursos
     */
    public function getCursoid(): Cursos
    {
        return $this->cursoid;
    }

    /**
     * @param Cursos $cursoid
     */
    public function setCursoid(Cursos $cursoid)
    {
        $this->cursoid = $cursoid;
    }
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var float|null
     *
     * @ORM\Column(name="nota_media", type="float", precision=10, scale=0, nullable=true)
     */
    private $notaMedia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password", type="string", length=50, nullable=true)
     */
    private $password;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="admin", type="boolean", nullable=true)
     */
    private $admin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="foto_perfil", type="blob", length=65535, nullable=true)
     */
    private $fotoPerfil;

    /**
     * @var \Cursos
     *
     * @ORM\ManyToOne(targetEntity="Cursos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursoID", referencedColumnName="id")
     * })
     */
    private $cursoid;
}