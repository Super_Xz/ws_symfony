<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerPdUkDxf\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerPdUkDxf/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerPdUkDxf.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerPdUkDxf\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerPdUkDxf\App_KernelDevDebugContainer([
    'container.build_hash' => 'PdUkDxf',
    'container.build_id' => 'fb4482b9',
    'container.build_time' => 1603364975,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerPdUkDxf');
